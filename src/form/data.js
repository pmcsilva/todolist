import React from "react";
import { Table, Button } from "reactstrap";

function buttonColor(item) {
  return item.complete === true ? "success" : "warning";
}

function Data(props) {
  return (
    <Table hover striped color="primary">
      <tbody>
        {props.tasksArray.map((item) => (
          <tr>
            <td>{props.tasksArray.indexOf(item)+1}</td>
            <td>{item.value}</td>
            <td align="justify">
              <Button
                color={buttonColor(item)}
                onClick={() => {
                  props.onTaskUpdate(item);
                }}
              >
                {item.complete ? "Complete" : "Incomplete"}
              </Button>
            </td>
            <td align="justify">
              <Button
                color="danger"
                onClick={() => {
                  props.onDelete(item);
                }}
              >
                <i className="far fa-trash-alt"></i>
              </Button>
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
}

export default Data;
