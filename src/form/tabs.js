import React from "react";
import {
  Row,
  Col,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
} from "reactstrap";
import classNames from "classnames";
import Data from "./data";

class Tabs extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTab: "1",
    };
  }

  toggle = (tab) => {
    if (this.state.activeTab !== tab) {
      this.setState({ activeTab: tab });
    }
  };

  filteredArray = (arr, arrFilter) => {
	const filtArray = arr;
	if(arrFilter === ""){
		return filtArray
	}
    return filtArray.filter((task) => task.complete === arrFilter);
  };

  panes = (tabNumber, tskArray, taskState ) => {
	  return(
		<TabPane tabId= {tabNumber}>
		<Row>
		  <Col sm="12">
			<Data
			  tasksArray={this.filteredArray(tskArray, taskState)}
			  onTaskUpdate={this.props.onTaskUpdate}
			  onDelete={this.props.onDelete}
			/>
		  </Col>
		</Row>
	  </TabPane>
	  );
  }

  navItems = (tabNumber, tabName) => {
	return(
		<NavItem>
			<NavLink
        className={classNames({ active: this.state.activeTab === tabNumber })}
        onClick={() => {
          this.toggle(tabNumber);
        }}
        >
        {tabName}
			</NavLink>
	  	</NavItem>
	);
  }

  render() {
    const {tasksArray} = this.props;

    return (
      <div>
        <Nav tabs pills justified>
			{this.navItems("1", "All")}
			{this.navItems("2", "Complete")}
			{this.navItems("3", "Incomplete")}
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
			  {this.panes("1", tasksArray, "")}
			  {this.panes("2", tasksArray, true)}
			  {this.panes("3", tasksArray, false)}
        </TabContent>
      </div>
    );
  }
}

export default Tabs;
