import React, { useState } from "react";
import { Button, FormGroup } from "reactstrap";
import {
  AvGroup,
  AvInput,
  AvForm,
  AvFeedback,
} from "availity-reactstrap-validation";

function TodoInput (props){
  /*constructor(props) {
    super(props);
    this.state = {
      taskId: 1,
    };
  }*/

  const [taskId, taskIdIn] = useState(0);

  function handleSubmit (event, values){
    //event.preventDefault();

    const {taskInput: value} = values;
    taskIdIn(taskId + 1);
   
    const task = {
      taskId,
      value,
      complete: false,
    };

    //this.setState({ taskId: this.state.taskId + 1});

    props.onSubmit(task);
    /*this.form && this.form.reset();
    ref={(c) => (this.form = c)}*/
  };
  
    return (
      <div>
        <AvForm onValidSubmit={handleSubmit}> 
          <AvGroup>
            <AvInput
              name="taskInput"
              placeholder="Task description"
              type="text"
              required
            />
            <AvFeedback>No task was given</AvFeedback>
          </AvGroup>
          <FormGroup>
            <Button color="primary">Add to list</Button>
          </FormGroup>
        </AvForm>
      </div>
    );
  }

export default TodoInput;
