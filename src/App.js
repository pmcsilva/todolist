import React from "react";
import { Container, Col, Row, Badge } from "reactstrap";
import "./App.css";
import TodoInput from "./form/input.js";
import Tabs from "./form/tabs";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
    };
  }

  handleNewTask = (task) => {
    this.setState((prevState) => ({ list: [...prevState.list, task] }));
  };

  handleChangeTaskStatus = (task) => {
    this.setState((prevState) => {
      const list = prevState.list.map((newStatusTask) =>
        newStatusTask.taskId === task.taskId
          ? { ...newStatusTask, complete: !newStatusTask.complete }
          : newStatusTask
      );
      return { list };
    });
  };

  handleDelete = (task) => {
    this.setState((prevState) => {
      const list = prevState.list.filter(
        (taskToDelete) => taskToDelete.taskId !== task.taskId
      );
      return { list };
    });
  };

  render() {
    return (
      <Container className="themed-container" fluid="sm">
        <Row>
          <Col sm="12" md={{ size: 6, offset: 3 }}>
            <Badge color="dark" pill>
              <h1>Todo list </h1>
            </Badge>
          </Col>
        </Row>
        <Row>
          <Col className="inputBox" sm="12" md={{ size: 6, offset: 3 }}>
            <TodoInput onSubmit={this.handleNewTask} />
          </Col>
        </Row>
        <Row>
          <Col sm="12" md={{ size: 6, offset: 3 }}>
            <Tabs
              tasksArray={this.state.list}
              onTaskUpdate={this.handleChangeTaskStatus}
              onDelete={this.handleDelete}
            />
          </Col>
        </Row>
      </Container>
    );
  }
}

export default App;
